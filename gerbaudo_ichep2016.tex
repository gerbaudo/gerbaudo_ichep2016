% Please use the skeleton file you have received in the
% invitation-to-submit email, where your data are already
% filled in. Otherwise please make sure you insert your
% data according to the instructions in PoSauthmanual.pdf
\documentclass{PoS}
\usepackage{amsmath}

%% \usepackage{lineno}
%% \linenumbers

% maximum lenght: 5 pages (not including title/abstract page)
\newcommand{\etal}{\emph{et al.}}
\newcommand{\pt}{\ensuremath{p_{T}}}
\newcommand{\met}{\ensuremath{E_{T}^{\text{miss}}}}
\newcommand{\tauell}{\ensuremath{\tau{}\rightarrow{}\ell{}\nu{}\bar{\nu{}}}}
\newcommand{\tauhad}{\ensuremath{\tau{}\rightarrow{}\text{hadrons}}}
\newcommand{\tauh}{\ensuremath{\tau{}_{\text{had}}}}
\newcommand{\taul}{\ensuremath{\tau{}_{\text{lep}}}}
\newcommand{\hgammagamma}{\ensuremath{H \rightarrow{}\gamma{}\gamma{}}}
\newcommand{\hzgamma}{\ensuremath{H \rightarrow{}Z\gamma{}}}
\newcommand{\hqgamma}{\ensuremath{H \rightarrow{}Q\gamma{}}}
\newcommand{\hjpsigamma}{\ensuremath{H \rightarrow{}\text{J}/\Psi{}\gamma{}}}
\newcommand{\hupsilongamma}{\ensuremath{H \rightarrow{}\Upsilon{}\gamma{}}}
\newcommand{\hphigamma}{\ensuremath{H \rightarrow{}\phi{}\gamma{}}}
\newcommand{\zqgamma}{\ensuremath{Z \rightarrow{}Q\gamma{}}}
\newcommand{\zjpsigamma}{\ensuremath{Z \rightarrow{}\text{J}/\Psi{}\gamma{}}}
\newcommand{\zupsilongamma}{\ensuremath{Z \rightarrow{}\Upsilon{}\gamma{}}}
\newcommand{\zphigamma}{\ensuremath{Z \rightarrow{}\phi{}\gamma{}}}
\newcommand{\htaue}{\ensuremath{H \rightarrow \tau{}e}}
\newcommand{\htaumu}{\ensuremath{H \rightarrow \tau{}\mu{}}}
\newcommand{\htautau}{\ensuremath{H \rightarrow \tau{}\tau{}}}
\newcommand{\zll}{\ensuremath{Z \rightarrow \ell{}\ell{}}}
\newcommand{\mtthad}{\ensuremath{m_{T}\left(\tau{}, \met{}\right)}}
\newcommand{\mtlep}{\ensuremath{m_{T}\left(\ell{}, \met{}\right)}}

\title{Search for non-standard and rare decays of the Higgs boson with the ATLAS detector}

\ShortTitle{ATLAS non-standard and rare decays of the Higgs boson}

\author{\speaker{Davide Gerbaudo}\thanks{On behalf of the ATLAS Collaboration}\\
        IFAE Barcelona, Spain\\
        E-mail: \email{gerbaudo@cern.ch}}

\abstract{At the LHC, searches for rare and exotic decays of the Higgs boson
constitute an excellent opportunity to look for new physics. Some
theories predict exotic decays that are heavily suppressed in the
Standard Model, such as the ones violating lepton flavor
conservation. Other theories predict enhanced branching ratios for
rare decay modes, such as the ones to a photon and a $Z$ boson, or to a
photon and a meson. Recent searches performed with $\sqrt{s}=8$~TeV and
$\sqrt{s}=13$~TeV $pp$ collisions recorded by the ATLAS detector are presented.}

\FullConference{38th International Conference on High Energy Physics\\
		3-10 August 2016\\
		Chicago, USA}


\begin{document}

\section{Introduction}
The CMS and ATLAS collaborations have studied in great detail the
$125$~GeV boson whose discovery they announced on the $4^\text{th}$ of
July, 2012. So far, this particle has shown all the characteristics of
a Standard Model (SM) Higgs boson. In particular, its couplings to
heavy particles have been probed through its decays to $\tau{}\tau{}$,
$bb$, $WW$, and $ZZ$, and they were found to be consistent with the SM
prediction.

The fit to obtain the coupling constants to SM fermions and gauge
bosons can constrain, with some dependence on the assumptions made in
the fit, the branching ratio (Br) for invisible or undetected Higgs
boson decays. From this fit, the $95\%$~CL upper limit on the Br of the
Higgs boson decaying to invisible or undetected final states can be as
large as $\approx{}30\%$~\cite{Khachatryan:2016vau}. Such a loose
constraint leaves open the possibility that the SM rare decays that
have not been observed yet could be anomalously enhanced, or the
possibility that unforeseen exotic decays might have sizeable
branching ratios.

Several theories beyond the SM---including SUSY, Higgs doublet models,
composite Higgs models---predict such exotic decays or such anomalous
enhancements. Since the current constraint leave ample room for both
phenomena, direct searches for exotic and rare decays of the Higgs
boson constitute a very powerful approach to look for physics beyond
the SM.
We report here searches performed with $pp$ collision data recorded by
the ATLAS detector~\cite{Aad:2008zzm} at $\sqrt{s}=8$~TeV and
$\sqrt{s}=13$~TeV.

%% Rare Higgs boson decays require larger datasets to be
%% probed, and they are still being searched for. These include
%% \hzgamma{}, \hjpsigamma{}, \hupsilongamma{}, and \hphigamma{}.

\section{Searches for Exotic Decays Violating Lepton Flavor Conservation}
Lepton-flavor-violating (LFV) decays are usually translated in a
Lagrangian
\begin{eqnarray}
  \mathcal{L}_{Y} =
  Y_{ij}\left(\overline{f}_L^i f_{R}^i\right)h
  + h.c. + \ldots{} \nonumber
\end{eqnarray}
where, following the notation used in Ref.~\cite{Harnik:2012pb} the
Yukawa terms $Y_{ij}$ are diagonal in the SM, and the off-diagonal
elements are non-zero in several theories beyond the SM. When that is the case,
diagrams corresponding to LFV processes arise. The amplitude of these
diagrams, or of the corresponding Yukawa terms $Y_{ij}$, are
constrained by experimental searches for several low-energy LFV
processes. These constraints are described in detail in the
paper~\cite{Harnik:2012pb} by Harnik \etal{}
%
The $e\mu{}$ Yukawa term is constrained by the upper limits from the
$\mu\to{}e\gamma$ searches, allowing $\text{Br}(H\to\mu{}e)$ to be
at most $\approx{}10^{-8}$. The constraints from $\tau\to\mu\gamma$ and
$\tau\to{}e\gamma$ are much weaker, allowing for
$\text{Br}(H\to\mu\tau)$ and $\text{Br}(H\to{}e\tau)$ to be as large
as $\approx{}10\%$.
In fact, in the paper~\cite{Harnik:2012pb} by Harnik \etal{} the most
stringent constraints on these decays are obtained recasting the
ATLAS \htautau{} search~\cite{Aad:2012mea}. % drop this last sentence?

%% What does a decay of a Higgs boson to a (light) lepton and a $\tau{}$
%% would look like?  It would be very similar to SM \htautau{}, but
%% because the leptons comes directly from the Higgs boson there would be
%% significant kinematic differences: the lepton would have a harder
%% \pt{}, and the only missing energy would be in the direction of the
%% $\tau{}$ lepton.

ATLAS has performed searches~\cite{Aad:2015gha,Aad:2016blu} for the
decays \htaumu{} and \htaue{} using both \tauell{} and \tauhad{}. Two
different search strategies are used in the hadronic channel and in
the leptonic one.

In the hadronic channel, events are selected by requiring some missing
transverse momentum \met{}, one light lepton $\ell{}$, and one
hadronic tau \tauh{}.  The lepton and the tau are required to be
well-separated and to have opposite charge. Based on the values of the
two transverse masses \mtthad{} and \mtlep{}, events are categorised
in a control region used to normalise the $W$+jets background, and two
signal regions populated by different background components. The
backgrounds are modelled with a combination of data-driven techniques
and simulation: the former for the $Z/\gamma^*\to\tau{}\tau{}$ and
multi-jet processes, and the latter for all the other processes. The
mass of the Higgs boson candidate is estimated with the Missing Mass
Calculator algorithm~\cite{Elagin:2010aw}, whose output is used
as a discriminant.

In the leptonic channel, events are selected by requiring two
opposite-sign, different-flavor leptons $e^{\pm{}}\mu{}^{\mp{}}$, as
well as angular cuts between the leptons and the missing transverse
energy. Events are split into categories based on the number of jets
in the event; this provides signal regions populated by different
Higgs production mechanisms. All backgrounds are estimated with
data-driven techniques, including the asymmetry method proposed in
Ref.~\cite{Bressler:2014jta}. The collinear mass, computed from the
two leptons and the missing transverse momentum, is used to
discriminate signal events from the background ones.
%% is strategy of this search is different from the one with teh
%% hadronic $\tau{}$ because here we use a data-driven asymmetry
%% method~\cite{Bressler:2014jta} proposed by Bressler \etal{} What we
%% do with this method is that we assume that the background events
%% due to the SM processes are symmetric when exchanging
%% $e\leftrightarrow{}\mu$, that is the events where the muon is the
%% highest-\pt{} lepton have the same kinematic of the events where
%% the electron is the highest-\pt{} lepton.

Combining the hadronic channel with the leptonic one, the $95\%$ CL
upper limit on Br(\htaue{}) is found to be $1.04\%$ ($1.21\%$
expected), and the one on Br(\htaumu{}) is found to be $1.43\%$
($1.01\%$ expected). The upper limits for each channel and for their
combination are summarised in Figure~\ref{fig:hlfvlimits}.
%
\begin{figure}
  \begin{center}
    \includegraphics[width=0.495\textwidth]{fig_05a_hlfv}
    \includegraphics[width=0.495\textwidth]{fig_05b_hlfv}
    \caption{
      \label{fig:hlfvlimits}
      Upper limits on LFV decays of the Higgs boson from
      Ref.~\cite{Aad:2016blu}.  On the left: limits on Br(\htaue{})
      are computed under the assumption that Br(\htaumu{})=0. On the
      right: limits on Br(\htaumu{}) are computed under the assumption
      that Br(\htaue{})=0.
    }
  \end{center}
\end{figure}


\section{Searches for Rare Decays of the Higgs Boson}
ATLAS has performed several searches for rare SM decays of the Higgs
boson.  Some of them have been shown at this conference in dedicated
presentations---see for example the one on $H\rightarrow{}\mu{}\mu{}$
by Christian Grefe.
%
In this contribution we consider the searches for decays having final
states with $\gamma{}+X$. In the $Z\gamma{}$ final state, the photon
comes directly from the decay of the Higgs boson; in the $Q+\gamma{}$
final state the photon is radiated by one of the quarkonium's quarks.

The search for the rare decay \hzgamma{} is performed using
$e^+e^-\gamma{}$ and $\mu^+\mu^-\gamma{}$ events recorded at
$\sqrt{s}=7$~TeV and $\sqrt{s}=8$~TeV.
%
Events are categorised based on the lepton flavor, the rapidity
difference between the $Z$ candidate and the photon, and the Higgs
boson candidate \pt{}. The $Z$-boson mass constraint is used in a
kinematic fit to estimate the mass of the Higgs boson candidate, which
is used as the discriminating variable between signal and background.
This search is currently sensitive to a branching ratio about ten
times larger than the SM one, as shown in
Figure~\ref{fig:fig_03_hzgam}.
%
\begin{figure}
  \begin{center}
    \includegraphics[width=0.495\textwidth]{fig_03hzgam.eps}
    \caption{
      \label{fig:fig_03_hzgam}
      Upper limits at $95\%$~CL on the decays of the Higgs boson to a
      photon and a $Z$ boson, from Ref.~\cite{Aad:2014fia}.
    }
  \end{center}
\end{figure}

Searches for decays of the Higgs boson to a quarkonium and a photon
are interesting because they make it possible to probe the couplings
of the boson to the $c$, $s$, and $b$ quarks. ATLAS has performed
searches for \hjpsigamma{}, \hupsilongamma{}, and \hphigamma{}. The
strategy for these searches is to look for events with the quarkonium
recoiling against a high-\pt{} photon. This strategy also makes it
possible to search for the rare decays \zjpsigamma{},
\zupsilongamma{}, and \zphigamma{}, which have not been observed yet.

In the searches for \hjpsigamma{} and \hupsilongamma{} the quarkonium
is reconstructed as a $\mu^+\mu{}^-$ pair.
%% Here we consider only the $\mu{}\mu{}\gamma{}$ final state because the
%% $ee\gamma{}$ is more challenging and it has poorer mass resolution.
The events, that were recorded in $pp$ collisions at $\sqrt{s}=8$~TeV,
are categorised based on whether the photon converted within the inner
detector, and on the pseudorapidity of the muons. A simultaneous
unbinned fit is performed on the $\pt\left(\mu\mu\right)$ and
$m\left(\mu\mu\gamma\right)$ distributions; for $\Upsilon\gamma{}$
candidates the $m\left(\mu\mu\right)$ distribution is also used in the
fit to discriminate between the three $\Upsilon\left(nS\right)$
states. The $m\left(\mu\mu\gamma\right)$ distribution provides
discrimination between signal and background. As shown in
Figure~\ref{fig:fig_03_hzqgam}, no significant excess is observed in
any of the hypotheses considered in this search. For all hypotheses,
the $95\%$~CL upper limits are larger than the ones predicted by the
SM; the process whose sensitivity is nearest to the SM expectation is
\hqgamma{}, for which the upper limit is set to be $540$ times the
expected rate. % rephrase
%
\begin{figure}
  \begin{center}
    \includegraphics[width=0.650\textwidth]{fig_03_hzqgam}
    \caption{
      \label{fig:fig_03_hzqgam}
      Upper limits on the decays of the Higgs boson and of the $Z$
      boson to a photon and a $J/\Psi{}$, or a photon and
      $\Upsilon{}\gamma{}$, from Ref.~\cite{Aad:2015sda}.
      % figure is actually from https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2014-03/
    }
  \end{center}
\end{figure}

Recently ATLAS has also performed a search~\cite{Aaboud:2016rug} for
the Higgs boson decaying to a photon and $\phi{}$ meson. In this
search the $\phi{}$ meson is reconstructed through its decay to
$K^+K^-$ that are identified as two high-\pt{} isolated collinear
tracks with opposite charge. The $13$~TeV dataset used in this study
was recorded at $\sqrt{s}=13$~TeV using a dedicated trigger requiring
an isolated high-\pt{} photon and a pair of tracks with an invariant
mass consistent with $m_{\phi}$.
%
The invariant mass $m\left(K^+K^-\gamma\right)$, whose distribution is shown in
Figure~\ref{fig:fig_03_hzphigam}, provides discrimination between
signal and background, which is estimated using a non-parametric
data-driven template technique similar to the one used in the
search~\cite{Aad:2015sda} for \hjpsigamma{} and \hupsilongamma{}. This
approach makes it possible to probe not only the decay \hphigamma{}
but also the decay \zphigamma{}, which has not been unobserved yet.
The $95\%$~CL upper limit set on Br$\left(\hphigamma{}\right)$ is
about $600$ times larger than the expected SM Br.  The $95\%$~CL upper
limit set on Br$\left(\zphigamma{}\right)$ is about $700$ times larger
than the expected SM Br.
%
\begin{figure}
  \begin{center}
    \includegraphics[width=0.495\textwidth]{fig_03_hzphigam}
    \caption{
      \label{fig:fig_03_hzphigam}
      From Ref.~\cite{Aaboud:2016rug}, $m\left(K^+K^-\gamma\right)$
      distribution of the $\phi\gamma{}$ candidates used in the search
      for \hphigamma{} and \zphigamma{}.
      The bottom plot illustrates the agreement of data with the
      background-only model.
    }
  \end{center}
\end{figure}

\section{Conclusion}


Using $pp$ collision data recorded at the LHC at $\sqrt{s}=8$ and
$\sqrt{s}=13$~TeV, ATLAS has carried out several searches for exotic and rare
decays of the Higgs boson and of the $Z$ boson. The former include:
\htaue{} and \htaumu{}. The latter include: \hzgamma{},
%
$H/Z\rightarrow{}\text{J}/\Psi{}\gamma{}$,
%
$H/Z\rightarrow{}\Upsilon{}\gamma{}$,
%
$H/Z\rightarrow{}\phi{}\gamma{}$.
%
In none of these searches a significant excess was observed. The
resulting upper limits allow to constrain several theories beyond the
SM. Larger datasets will be needed to reach the sensitivity required
to probe the Br predicted by the SM for rare decays of the Higgs boson
and of the $Z$ boson.

\section{Acknowledgements}
The participation of the author to this conference was supported with
funds from the MINECO under the Juan de la Cierva-Incorporaci{\'{o}}n
Programme (IJCI-2014-21919).


\begin{thebibliography}{99}
\bibitem{Khachatryan:2016vau}
  ATLAS and CMS Collaborations,
  \emph{{Measurements of the Higgs boson production and decay rates and constraints on its couplings from a combined ATLAS and CMS analysis of the LHC pp collision data at $\sqrt{s}=7$ and $8$~TeV}},
  \emph{JHEP} {\bf 1608} (2016) 045,
       {\tt arXiv:1606.02266 [hep-ex]}
\bibitem{Aad:2008zzm}
  ATLAS Collaboration,
  \emph{{The ATLAS Experiment at the CERN Large Hadron Collider}},
  \emph{JINST} {\bf 3} (2008) S08003
\bibitem{Harnik:2012pb}
  R. Harnik, J. Kopp, J. Zupan,
  \emph{{Flavor Violating Higgs Decays}},
  \emph{JHEP} {\bf 03}, (2013) 026,
       {\tt arXiv:1209.1397 [hep-ph]}
\bibitem{Aad:2012mea}
  ATLAS Collaboration,
  \emph{{Search for the Standard Model Higgs boson in the $H$ to $\tau^{+} \tau^{-}$ decay mode in $\sqrt{s}=7$~TeV $pp$ collisions with ATLAS}},
  \emph{JHEP} {\bf 1209} (2012) 070,
       {\tt arXiv:1206.5971 [hep-ex]}
\bibitem{Aad:2015gha}
  ATLAS Collaboration,
  \emph{{Search for lepton-flavour-violating \htaumu{} decays of the Higgs boson with the ATLAS detector}},
  \emph{JHEP} {\bf 1511} (2015) 211,
       {\tt arXiv:1508.03372 [hep-ex]}
\bibitem{Aad:2016blu}
  ATLAS Collaboration,
  \emph{{Search for lepton-flavour-violating decays of the Higgs and $Z$ bosons with the ATLAS detector}},
       {\tt arXiv:1604.07730 [hep-ex]}
\bibitem{Elagin:2010aw}
  A. Elagin, P. Murat, A. Pranko, A. Safonov,
  \emph{{A New Mass Reconstruction Technique for Resonances Decaying to di-tau}},
  \emph{Nucl.Instrum.Meth.} {\bf A654} (2011) 481-489,
       {\tt arXiv:1012.4686 [hep-ex]}
\bibitem{Bressler:2014jta}
  S. Bressler, A. Dery, A. Efrati,
  \emph{{Asymmetric lepton-flavor violating Higgs boson decays}},
  \emph{Phys.Rev.} {\bf D90} (2014), 015025,
       {\tt arXiv:1405.4545 [hep-ph]}
\bibitem{Aad:2014fia}
  ATLAS Collaboration,
  \emph{{Search for Higgs boson decays to a photon and a Z boson in pp collisions at $\sqrt{s}=7$~TeV and $8$~TeV with the ATLAS detector}},
  \emph{Phys.Lett.} {\bf B732} (2014) 8-27,
       {\tt arXiv:1402.3051 [hep-ex]}
\bibitem{Aad:2015sda}
  ATLAS Collaboration,
  \emph{{Search for Higgs and Z Boson Decays to J$/\Psi{}\gamma{}$ and $\Upsilon(nS)\gamma{}$ with the ATLAS Detector}},
  \emph{Phys.Rev.Lett.} {\bf 114} (2015), 121801,
       {\tt arXiv:1501.03276 [hep-ex]}.
       \\Auxiliary figures are available at this URL:
       {\tt https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HIGG-2014-03}
\bibitem{Aaboud:2016rug}
  ATLAS Collaboration,
  \emph{{Search for Higgs and Z Boson Decays to $\phi{}\gamma{}$ with the ATLAS Detector}},
  \emph{Phys.Rev.Lett.} {\bf 117} (2016), 111802,
       {\tt arXiv:1607.03400 [hep-ex]}
%% for kkgam it would be good to add a ref to the 2015 trigger paper.
%% ...too bad it's not public yet.
%
%% \bibitem{Atl:phys2013prj} % probably not needed unless you want to talk about projections
%%   ATLAS Collaboration,
%%   \emph{{Projections for measurements of Higgs boson cross sections, branching ratios and coupling parameters with the ATLAS detector at a HL-LHC}},
%%   \emph{ATL-PHYS-PUB-2013-014} (2013),

\end{thebibliography}

\end{document}

%%  LocalWords:  CMS Higgs bb ZZ fermions bosons SUSY Yukawa BSM MMC
%%  LocalWords:  Harnik Grefe quarkonium's quarkonium pseudorapidity
%%  LocalWords:  unbinned nS dataset JHEP Kopp Zupan ph Elagin Pranko
%%  LocalWords:  Safonov di Bressler Dery Efrati Lett
