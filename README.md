Proceedings for my contribution at ICHEP 2016.

Compile with

```
pdflatex gerbaudo_ichep2016.tex
```

Clean with

```
rm -f  $(cat .gitignore)
```

Tested with `pdfTeX 3.14159265-2.6-1.40.15 (TeX Live 2014)`

davide.gerbaudo@gmail.com
Nov 2016
